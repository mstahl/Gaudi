#ifndef GAUDIKERNEL_APPLY_H
#define GAUDIKERNEL_APPLY_H
#warning "Obsolete header, please use std::apply"
#include <tuple>
namespace Gaudi {
  using std::apply;
}
#endif
